package ${package}.adapter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.test.context.junit4.SpringRunner;

import ${package}.App;
import ${package}.config.broker.BrokerInput;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = WebEnvironment.MOCK)
public class AdapterEntradaBrokerTest {

	@Autowired
	BrokerInput input;

	@Test
	public void testReceiveMessage() {

		String json = "{\"status\": \"PENDING\"}";
		
		Message<String> enviada = MessageBuilder.withPayload(json).build();
		
		this.input.inputBroker().subscribe(new MessageHandler() {
			
			@Override
			public void handleMessage(Message<?> message) throws MessagingException {
				Message<String> expected = MessageBuilder.withPayload(json).build();

				assertEquals(expected.getPayload(), message.getPayload());
			}
		});
		input.inputBroker().send(enviada);
	}
	
}
