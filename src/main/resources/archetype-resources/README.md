#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

${symbol_pound} Credenciamento Digital Microservice

${symbol_pound}${symbol_pound} Tecnologias
- Spring Boot
- Swagger
- MongoDB

${symbol_pound}${symbol_pound} Pré-requisitos
- Java (versão 8 ou mais atual)
- Maven (versão 3 ou mais atual)
- MongoDB (versão 3.4 ou mais atual)

${symbol_pound}${symbol_pound} Desenvolvimento local

${symbol_pound}${symbol_pound}${symbol_pound} Rodando os testes
```
mvn clean install
```

${symbol_pound}${symbol_pound}${symbol_pound} Rodando a aplicação
```
java -jar target/<app-name>-${version}.jar
```

${symbol_pound}${symbol_pound} Rodando a aplicação em modo de produção
```
java -jar target/<app-name>-${version}.jar --spring.profiles.default=prod
```

${symbol_pound}${symbol_pound} Registrando aplicação no Consul
```
cd src/main/docker
curl --request PUT --data @microservice.json http://localhost:8500/v1/agent/service/register
```
