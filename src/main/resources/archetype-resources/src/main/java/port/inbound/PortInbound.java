package ${package}.port.inbound;

import java.util.List;

import ${package}.domain.Sample;

public interface PortInbound {
	
	public Sample save(Sample sample);

	public List<Sample> findAll();
	
	public Sample findOne(String id);
    
	public void delete(String id);

}
